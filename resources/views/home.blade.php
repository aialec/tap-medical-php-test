<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <title>Laravel</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">

    <!-- Styles -->
    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
          integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

    <style>
        html, body {
            background-color: #fff;
            height: 100vh;
            margin: 0;
        }

        .full-height {
            height: 100vh;
        }

        .flex-center {
            align-items: center;
            display: flex;
            justify-content: center;
        }

        .position-ref {
            position: relative;
        }

        .content {
            text-align: center;
        }

        .title {
            font-size: 84px;
        }

        .links > a {
            color: #636b6f;
            padding: 0 25px;
            font-size: 12px;
            font-weight: 600;
            letter-spacing: .1rem;
            text-decoration: none;
            text-transform: uppercase;
        }

        .m-b-md {
            margin-bottom: 30px;
        }
    </style>
    <script src="//code.jquery.com/jquery-1.10.2.js"></script>
    <script src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script>
    <script>
        $(function () {
            var selectedDoctor = null;
            var selectedDate = null;
            var maxDate = new Date();
            maxDate.setMonth(maxDate.getMonth() + 1);
            $("#datepicker").datepicker({
                dateFormat: 'yy-mm-dd',
                maxDate: maxDate,
                onSelect: function (dateText) {
                    selectedDate = dateText;
                }
            });

            $('#doctors_input').change(function () {
                selectedDoctor = $(this).val();
            });

            $('#submitBtn').click(function () {
                if (selectedDoctor !== null && selectedDate !== null) {
                    toggleSubmitButton(true);
                    $.get('/appointments/doctor/' + selectedDoctor + '/date/' + selectedDate)
                            .done(function (data) {
                                if (data.length > 0) {
                                    $('#appointments').html(generateAppointmentsHtml(data));
                                } else {
                                    alert("There are no appointments for selected doctor and date.");
                                    $('#appointments').html('');
                                }

                                toggleSubmitButton(false);
                            })
                            .fail(function(error){
                                alert("Ooops! Something went wrong.");
                                toggleSubmitButton(false);
                            });
                } else {
                    alert("All fields are required!");
                }
            });
        });

        function generateAppointmentsHtml(appointments) {
            var html = '';
            html += '<table class="table table-striped mt-5">';
            html += '<thead>';
            html += '<tr>';
            html += '<th scope="col">' + 'Time' + '</th>';
            html += '<th scope="col">' + 'Specialty name' + '</th>';
            html += '<th scope="col">' + 'Status' + '</th>';
            html += '<th scope="col">' + 'Patient' + '</th>';
            html += '</tr>';
            html += '</thead>';
            html += '</tbody>';
            appointments.forEach(function (appointment) {
                var startAt = (new Date(appointment.START_AT));
                var status = appointment.STATUS ? 'Incomming' : 'Cancelled';
                if(startAt.getTime() <= (new Date()).getTime()){
                    status = 'Passed';
                }
                html += '<tr>';
                html += '<td>' + startAt.toLocaleTimeString() + '</td>';
                html += '<td>' + appointment.doctor.SPECIALTY + '</td>';
                html += '<td>' + status + '</td>';
                html += '<td>' + appointment.patient.NAME + '</td>';
                html += '</tr>';
            });
            html += '</tbody>';
            html += '</table>';

            return html;
        }

        function toggleSubmitButton(isLoading){
            var $btn = $('#submitBtn');
            if(isLoading){
                $btn.attr('disabled', 'disabled');
                $btn.html('<span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>Loading...');
            } else {
                $btn.removeAttr('disabled');
                $btn.text('Get Appointments');
            }
        }
    </script>
</head>
<body>
<div class="flex-center position-ref full-height">

    <div class="content">
        <div class="title m-b-md">
            Children's department
        </div>

        <div class="container">
            <form novalidate action="javascript:void(0)">
                <div class="form-row align-items-center">
                    <div class="form-group col-auto">
                        <label for="doctors_input">Doctor</label>
                        <select id="doctors_input" class="form-control">
                            <option selected>Choose...</option>
                            @foreach($doctors as $doctor)
                                <option value='{{$doctor->ID}}'> {{$doctor->NAME}} ({{$doctor->SPECIALTY}})</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group col-auto">
                        <label>Date</label>
                        <input class="form-control" type="text" id="datepicker" value=""/></span>
                    </div>
                    <div class="col-auto">
                        <button id="submitBtn" class="btn btn-primary mt-3" type="submit">Get Appointments</button>
                    </div>
                </div>
            </form>

            <div id="appointments"></div>
        </div>
    </div>
</div>
</body>
</html>
