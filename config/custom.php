<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Application Name
    |--------------------------------------------------------------------------
    |
    | This value is the name of your application. This value is used when the
    | framework needs to place the application's name in a notification or
    | any other location as required by the application or its packages.
    */

    'dateFormat' => 'Y-m-d',
    'dateTimeFormat' => 'Y-m-d H:i:s',
    'dataSourceEndpoint' => 'http://ch-api-test.herokuapp.com',
    'basicAuthUserName' => 'myah',
    'basicAuthPassword' => '654321',
    'jwtAuthEmail' => 'zlindgren@yahoo.com',
    'jwtAuthPassword' => 'Tap!M3d1cal'

];