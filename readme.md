## CronJob

```
* * * * * cd /path-to-your-project && php artisan schedule:run >> /dev/null 2>&1
```
### Run manually
```
php artisan pull:appointments
```

## Database

```
CREATE DATABASE `forge`;

CREATE TABLE `appointments` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ID_SOURCE` int(11) NOT NULL,
  `STATUS` tinyint(4) NOT NULL,
  `START_AT` datetime NOT NULL,
  `ID_DOCTOR` int(11) NOT NULL,
  `ID_PATIENT` int(11) NOT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `ID_SOURCE_2` (`ID_SOURCE`),
  KEY `ID_DOCTOR` (`ID_DOCTOR`),
  KEY `ID_SOURCE` (`ID_SOURCE`),
  KEY `ID_PATIENT` (`ID_PATIENT`),
  CONSTRAINT `fk_appointment_doctor` FOREIGN KEY (`ID_DOCTOR`) REFERENCES `doctors` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_appointment_patient` FOREIGN KEY (`ID_PATIENT`) REFERENCES `patients` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=3244 DEFAULT CHARSET=latin1;

CREATE TABLE `doctors` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `NAME` varchar(500) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `SPECIALTY` varchar(3000) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `ID_SOURCE` int(11) NOT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `ID_SOURCE_2` (`ID_SOURCE`),
  KEY `ID_SOURCE` (`ID_SOURCE`)
) ENGINE=InnoDB AUTO_INCREMENT=95 DEFAULT CHARSET=latin1;

CREATE TABLE `patients` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ID_SOURCE` int(11) NOT NULL,
  `NAME` varchar(500) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `DATE_OF_BIRTH` date NOT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `ID_SOURCE_2` (`ID_SOURCE`),
  KEY `ID_SOURCE` (`ID_SOURCE`)
) ENGINE=InnoDB AUTO_INCREMENT=1373 DEFAULT CHARSET=latin1;
```

# Run

```
php artisan serve
```