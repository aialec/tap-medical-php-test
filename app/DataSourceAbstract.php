<?php

namespace App;

use App\Interfaces\AuthProviderInterface;
use App\Interfaces\DataSourceInterface;
use PhpParser\Error;


abstract class DataSourceAbstract implements DataSourceInterface
{
    private $authProvider;
    private $endpoint;

    public function __construct(AuthProviderInterface $authProvider, $endpoint)
    {
        $this->authProvider = $authProvider;
        $this->endpoint = $endpoint;
    }

    public function getAuthProvider(){
        return $this->authProvider;
    }

    public function getEndpoint(){
        return $this->endpoint;
    }

    public function getSyncData($startDateStr){
        throw new Error("Method not implemented");
    }
}