<?php

namespace App;

use \App\Interfaces\AuthProviderInterface;

class XMLDataSource extends DataSourceAbstract {

    public function __construct(AuthProviderInterface $authProvider, $endpoint){
        parent::__construct($authProvider, $endpoint);
    }

    public function getSyncData($startDateStr){
        $startDate = new \DateTime($startDateStr);
        $endDate = (new \DateTime())->modify('+1 month');

        $importData = [];

        // paging
        for($i = $startDate; $i <= $endDate; $i->modify('+2 day')){
            $client = new \GuzzleHttp\Client();
            try {
                $response = $client->get($this->getEndpoint(), [
                    'headers' => $this->getAuthProvider()->getAuthCredentials(),
                    'query' => ["from" => $i->format(config('custom.dateTimeFormat'))]
                ]);
            } catch (\GuzzleHttp\Exception\ClientException  $e) {
                die("Error: Request failed");
            }

            $nextImportData = $this->parseXML((string)$response->getBody());

            $importData = array_merge($importData, $nextImportData);
        }

        return $importData;
    }

    private function parseXML($xmlStr){
        $xmlData = simplexml_load_string($xmlStr) or die("Error: Cannot create object");
        $parsedData = [];
        foreach($xmlData as $xmlEntry){
            if($xmlEntry->count() > 0) {
                $dataModel = new SyncAppointmentsModel();
                $dataModel->setIdSource($xmlEntry->id->__toString());
                $dataModel->setStatus($xmlEntry->cancelled ? false : true);
                $dataModel->setStartAt((new \DateTime($xmlEntry->start_date->__toString() . " " . $xmlEntry->start_time->__toString()))->format(config('custom.dateTimeFormat')));
                $dataModel->setIdSourceDoctor($xmlEntry->doctor->id->__toString());
                $dataModel->setDoctorName($xmlEntry->doctor->name->__toString());
                $dataModel->setDoctorSpecialty($xmlEntry->specialty->name->__toString());
                $dataModel->setIdSourcePatient($xmlEntry->patient->id->__toString());
                $dataModel->setPatientName($xmlEntry->patient->name->__toString());
                $dataModel->setPatientDateOfBirth((new \DateTime($xmlEntry->patient->date_of_birth->__toString()))->format(config('custom.dateFormat')));

                array_push($parsedData, $dataModel);
            }
        }

        return $parsedData;
    }
}