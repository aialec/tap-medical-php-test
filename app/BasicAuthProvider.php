<?php

namespace App;
use App\Interfaces\AuthProviderInterface;


class BasicAuthProvider implements AuthProviderInterface
{
    private $username;
    private $password;
    public function __construct($username, $password)
    {
        $this->username = $username;
        $this->password = $password;
    }

    public function getAuthCredentials(){
        return ['Authorization' => 'Basic '. base64_encode($this->username.':'.$this->password)];
    }

    public function onCredentialsDenied(){
        // TODO report
    }
}