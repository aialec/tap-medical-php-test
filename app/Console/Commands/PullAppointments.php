<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Services\SyncAppointmentsService;

class PullAppointments extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'pull:appointments';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Pull appointments from all sources';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
//        $startDateStr = (new \DateTime('now'))->format(app('config')->get('custom')['dateTimeFormat']);
        $startDateStr = "2019-04-15 14:15:00";
        $syncAppointmentsService = new SyncAppointmentsService();
        $syncAppointmentsService->syncData($startDateStr);
    }
}
