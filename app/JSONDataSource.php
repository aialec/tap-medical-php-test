<?php

namespace App;

use \App\Interfaces\AuthProviderInterface;

class JSONDataSource extends DataSourceAbstract {

    public function __construct(AuthProviderInterface $authProvider, $endpoint){
        parent::__construct($authProvider, $endpoint);
    }

    public function getSyncData($startDateStr){
        $startDate = new \DateTime($startDateStr);

        $importData = [];

        // paging
        $page = 1;
        $keepGoing = true;
        while ($keepGoing) {
            $client = new \GuzzleHttp\Client();
            try {
                $response = $client->get($this->getEndpoint(), [
                    'headers' => ['Authorization' => $this->getAuthProvider()->getAuthCredentials()],
                    'query' => ["from" => $startDate->format(config('custom.dateTimeFormat')), "page" => $page]
                ]);
            } catch (\GuzzleHttp\Exception\ClientException  $e) {
                if ($e->hasResponse()) {
                    $response = $e->getResponse();
                    if ($response->getStatusCode() === 400) {
                        $this->getAuthProvider()->onCredentialsDenied();
                        continue;
                    } else {
                        die("Error: Request failed");
                    }
                }

                die("Error: Request failed");
            }

            $data = json_Decode((string)$response->getBody());
            $nextImportData = $this->parseJSON($data->data);

            $importData = array_merge($importData, $nextImportData);

            if ($data->last_page === $page) {
                $keepGoing = false;
            } else {
                $page++;
            }
        }

        return $importData;
    }

    private function parseJSON($json){
        $parsedData = [];

        foreach($json as $jsonEntry){
            $dataModel = new SyncAppointmentsModel();
            $dataModel->setIdSource($jsonEntry->id);
            $dataModel->setStatus($jsonEntry->status === "booked" ? true : false);
            $dataModel->setStartAt((new \DateTime($jsonEntry->datetime))->format(config('custom.dateTimeFormat')));
            $dataModel->setIdSourceDoctor($jsonEntry->doctor->id);
            $dataModel->setDoctorName($jsonEntry->doctor->name);
            $dataModel->setDoctorSpecialty($jsonEntry->specialty->name);
            $dataModel->setIdSourcePatient($jsonEntry->patient->id);
            $dataModel->setPatientName($jsonEntry->patient->name);
            $dataModel->setPatientDateOfBirth((new \DateTime($jsonEntry->patient->dob))->format(config('custom.dateFormat')));

            array_push($parsedData, $dataModel);
        }

        return $parsedData;
    }
}