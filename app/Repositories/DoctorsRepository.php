<?php

namespace App\Repositories;

use App\Models\Doctor;

class DoctorsRepository
{
    public function getDoctors(){
        return Doctor::all();
    }
}