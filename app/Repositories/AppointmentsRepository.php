<?php

namespace App\Repositories;

use App\SyncAppointmentsModel;
use App\Models\Appointment;

class AppointmentsRepository
{
    public function getAppointmentsByDoctorForDate($doctorId, $date){
        return Appointment::with('doctor', 'patient')
            ->where('ID_DOCTOR', $doctorId)
            ->whereDate('START_AT', '=', $date)
            ->orderBy('START_AT', 'asc')
            ->get();
    }

    public function syncAppointments($startDateStr, $syncData){
        $dataFiltered = $this->applyFilter($startDateStr, $syncData);

        \Illuminate\Support\Facades\DB::transaction(function () use ($dataFiltered) {
            foreach($dataFiltered as $entry){
                $this->syncAppointment($entry);
            }
        }, 3);
    }

    private function syncAppointment(SyncAppointmentsModel $entry){
        $appointment = \App\Models\Appointment::where('ID_SOURCE', $entry->getIdSource())->first();
        $doctor = \App\Models\Doctor::firstOrCreate(['ID_SOURCE' => $entry->getIdSourceDoctor()],
            ['ID_SOURCE' => $entry->getIdSourceDoctor(), 'NAME' => $entry->getDoctorName(), 'SPECIALTY' => $entry->getDoctorSpecialty()]);
        $patient = \App\Models\Patient::firstOrCreate(['ID_SOURCE' => $entry->getIdSourcePatient(), 'NAME' => $entry->getPatientName(), 'DATE_OF_BIRTH' => $entry->getPatientDateOfBirth()]);

        if($appointment === null){
            \App\Models\Appointment::create([
                'ID_SOURCE' => $entry->getIdSource(),
                'STATUS' => $entry->getStatus(),
                'START_AT' => $entry->getStartAt(),
                'ID_DOCTOR' => $doctor->ID,
                'ID_PATIENT' => $patient->ID
            ]);
        }
    }

    private function applyFilter($startDateStr, $array)
    {
        return array_filter($array, function(SyncAppointmentsModel $element) use ($startDateStr){
            $underAge = 18;
            $endDate = (new \DateTime($startDateStr))->modify('+1 month');

            $today = new \DateTime(date(config('custom.dateFormat')));
            $dateOfBirth = new \DateTime($element->getPatientDateOfBirth());
            $interval = $today->diff($dateOfBirth);

            // if under 18 and appointment date is max 1 month in advance
            if(intval($interval->y) < $underAge && new \DateTime($element->getStartAt()) <= $endDate){
                return true;
            }

            return false;
        });
    }
}