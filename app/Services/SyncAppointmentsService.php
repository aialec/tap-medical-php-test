<?php

namespace App\Services;
//use App\SyncAppointmentsModel;
use App\Repositories\AppointmentsRepository;

/**
 * Created by PhpStorm.
 * User: alec
 * Date: 29-Oct-19
 * Time: 20:27
 */
class SyncAppointmentsService
{
    public function syncData($startDateStr){
        $endPointAll = config('custom.dataSourceEndpoint');

        $dataSources = [
            new \App\XMLDataSource(
                new \App\BasicAuthProvider(config('custom.basicAuthUserName'), config('custom.basicAuthPassword')), $endPointAll."/xml"
            ),
            new \App\JSONDataSource(
                new \App\JwtAuthProvider($endPointAll."/auth", config('custom.jwtAuthEmail'), config('custom.jwtAuthPassword')), $endPointAll."/json"
            )
        ];

        $appointmentsRepo = new AppointmentsRepository();
        foreach($dataSources as $ds){
            $appointmentsRepo->syncAppointments($startDateStr, $ds->getSyncData($startDateStr));
        }
    }
}