<?php

namespace App\Interfaces;

interface AuthProviderInterface {

    public function getAuthCredentials();
    public function onCredentialsDenied();
}