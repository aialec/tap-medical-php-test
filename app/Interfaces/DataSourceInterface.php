<?php

namespace App\Interfaces;

interface DataSourceInterface {
    public function getSyncData($startDateStr);
    public function getAuthProvider();
    public function getEndpoint();
}