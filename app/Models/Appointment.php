<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Appointment extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'appointments';

    /**
     * The primary key associated with the table.
     *
     * @var string
     */
    protected $primaryKey = 'ID';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['ID_SOURCE', 'STATUS', 'START_AT', 'ID_DOCTOR', 'ID_PATIENT'];

    public function patient(){
        return $this->belongsTo('App\Models\Patient', 'ID_PATIENT');
    }

    public function doctor(){
        return $this->belongsTo('App\Models\Doctor', 'ID_DOCTOR');
    }
}
