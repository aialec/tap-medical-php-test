<?php

namespace App;
use App\Interfaces\AuthProviderInterface;


class JwtAuthProvider implements AuthProviderInterface
{
    private $endpoint;
    private $email;
    private $password;
    private $token = null;

    public function __construct($endpoint, $username, $password)
    {
        $this->endpoint = $endpoint;
        $this->email = $username;
        $this->password = $password;
    }

    public function getAuthCredentials(){
        return ['Authorization' => $this->getToken()];
    }

    private function getToken(){
        if($this->token){
            return $this->token;
        } else {
            $client = new \GuzzleHttp\Client();

            try {
                $response = $client->request('POST', $this->endpoint, ['form_params' => [
                    'email' => $this->email,
                    'password' => $this->password
                ]]);

                $resData = json_decode((string)$response->getBody());
                $this->token = $resData->token;
            } catch (\GuzzleHttp\Exception\ClientException  $e) {
                die("Error: Request failed");
            }

            return $this->token;
        }
    }

    public function onCredentialsDenied(){
        // reset token
        $this->token = null;
    }

}