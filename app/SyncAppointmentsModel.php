<?php

namespace App;

class SyncAppointmentsModel
{
    private $id_source;
    private $status;
    private $start_at;
    private $id_source_doctor;
    private $doctor_name;
    private $doctor_specialty;
    private $id_source_patient;
    private $patient_name;
    private $patient_date_of_birth;

    /**
     * @return mixed
     */
    public function getIdSource()
    {
        return $this->id_source;
    }

    /**
     * @param mixed $id_source
     */
    public function setIdSource($id_source)
    {
        $this->id_source = $id_source;
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param mixed $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * @return mixed
     */
    public function getStartAt()
    {
        return $this->start_at;
    }

    /**
     * @param mixed $start_at
     */
    public function setStartAt($start_at)
    {
        $this->start_at = $start_at;
    }

    /**
     * @return mixed
     */
    public function getIdSourceDoctor()
    {
        return $this->id_source_doctor;
    }

    /**
     * @param mixed $id_source_doctor
     */
    public function setIdSourceDoctor($id_source_doctor)
    {
        $this->id_source_doctor = $id_source_doctor;
    }

    /**
     * @return mixed
     */
    public function getDoctorName()
    {
        return $this->doctor_name;
    }

    /**
     * @param mixed $doctor_name
     */
    public function setDoctorName($doctor_name)
    {
        $this->doctor_name = $doctor_name;
    }

    /**
     * @return mixed
     */
    public function getDoctorSpecialty()
    {
        return $this->doctor_specialty;
    }

    /**
     * @param mixed $doctor_specialty
     */
    public function setDoctorSpecialty($doctor_specialty)
    {
        $this->doctor_specialty = $doctor_specialty;
    }

    /**
     * @return mixed
     */
    public function getIdSourcePatient()
    {
        return $this->id_source_patient;
    }

    /**
     * @param mixed $id_source_patient
     */
    public function setIdSourcePatient($id_source_patient)
    {
        $this->id_source_patient = $id_source_patient;
    }

    /**
     * @return mixed
     */
    public function getPatientName()
    {
        return $this->patient_name;
    }

    /**
     * @param mixed $patient_name
     */
    public function setPatientName($patient_name)
    {
        $this->patient_name = $patient_name;
    }

    /**
     * @return mixed
     */
    public function getPatientDateOfBirth()
    {
        return $this->patient_date_of_birth;
    }

    /**
     * @param mixed $patient_date_of_birth
     */
    public function setPatientDateOfBirth($patient_date_of_birth)
    {
        $this->patient_date_of_birth = $patient_date_of_birth;
    }
}