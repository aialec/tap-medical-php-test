<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Repositories\AppointmentsRepository;
use App\Repositories\DoctorsRepository;

Route::get('/', function () {
    $repo = new DoctorsRepository();

    return view('home', ['doctors' => $repo->getDoctors()]);
});

Route::get('/appointments/doctor/{doctorId}/date/{date}', function ($doctorId, $date) {
    $repo = new AppointmentsRepository();

    return response($repo->getAppointmentsByDoctorForDate($doctorId, $date), 200)
        ->header('Content-Type', 'application/json');
});
